﻿using System.ComponentModel.Composition;
using Prism.Modularity;
using Microsoft.Practices.ServiceLocation;
using Common;
using System.Windows.Controls;
using PluginPicture.Views;

namespace PluginPicture.ViewModels
{
    [Export(typeof(IPluginViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    [Module(ModuleName = "PicturePluginViewModel")]
    class PicturePluginViewModel : ViewModelBase, IPluginViewModel
    {
        [ImportingConstructor]
        public PicturePluginViewModel(IServiceLocator _serviceLocator)
        {
            Content = _serviceLocator.GetInstance<PictureView>();
        }

        public string Title => "Picture";

        public UserControl Content
        {
            get;
            set;
        }

        public void Initialize()
        {

        }
    }
}
