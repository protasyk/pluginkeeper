﻿using Common;
using System.ComponentModel.Composition;

namespace PluginPicture.ViewModels
{
    [Export]
    public class PictureViewModel: ViewModelBase
    {
        private int _width;
        private int _height;

        public PictureViewModel()
        {
            //initial with and height have to be taken away to some config or resource
            _width = 350;
            _height = 300;            
        }

        /// <summary>
        /// Picture width
        /// </summary>
        public int Width
        {
            get { return _width; }
            set
            {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        /// <summary>
        /// Picture height
        /// </summary>
        public int Height
        {
            get { return _height; }
            set
            {
                _height = value;
                RaisePropertyChanged("Height");
            }
        }
    }
}
