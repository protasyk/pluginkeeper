﻿using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace PluginPicture.Views
{
    /// <summary>
    /// Interaction logic for PictureView.xaml
    /// </summary>
    [Export]
    public partial class PictureView : UserControl
    {
        public PictureView()
        {
            InitializeComponent();
        }
    }
}
