﻿namespace PluginPicture.Resources
{
    public class ResourcesAccessor
    {
        private static readonly Resource _resource = new Resource();

        public Resource StringResources { get => _resource; }

    }
}
