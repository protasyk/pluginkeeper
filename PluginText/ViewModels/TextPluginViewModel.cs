﻿using Common;
using Microsoft.Practices.ServiceLocation;
using PluginText.Views;
using Prism.Modularity;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace PluginText.ViewModels
{
    [Export(typeof(IPluginViewModel))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    [Module(ModuleName = "TextPluginViewModel")]
    class TextPluginViewModel : ViewModelBase, IPluginViewModel
    {
        [ImportingConstructor]
        public TextPluginViewModel(IServiceLocator _serviceLocator)
        {
            Content = _serviceLocator.GetInstance<TextView>();
        }

        public string Title => "Text";

        public UserControl Content
        {
            get;
            set;
        }


        public void Initialize()
        {

        }
    }
}
