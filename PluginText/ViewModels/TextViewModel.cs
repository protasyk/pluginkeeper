﻿using Common;
using System.ComponentModel.Composition;

namespace PluginText.ViewModels
{
    [Export]
    public class TextViewModel : ViewModelBase
    {
        private CommandBase _addText;

        public TextViewModel()
        {    
            _addText = new CommandBase(DoAddTextAndlearInput);
        }

        /// <summary>
        /// Text summary
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Current text input
        /// </summary>
        public string Input { get; set; }

        /// <summary>
        /// Command for relocate text from input to summary
        /// </summary>
        public CommandBase AddText
        {
            get { return _addText; }
        }

        private void DoAddTextAndlearInput(object obj)
        {
            Summary += "\n" + Input;
            Input = string.Empty;
            RaisePropertyChanged("Summary");
            RaisePropertyChanged("Input");
        }
    }
}
