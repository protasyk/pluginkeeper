﻿using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace PluginText.Views
{
    /// <summary>
    /// Interaction logic for TextView.xaml
    /// </summary>
    [Export]
    public partial class TextView : UserControl
    {
        public TextView()
        {
            InitializeComponent();
        }
    }
}
