﻿using System;
using System.Windows.Controls;

namespace Common
{
    public interface IPluginViewModel 
    {
        /// <summary>
        /// Title for showing in plugin list
        /// </summary>
        public String Title { get; }

        /// <summary>
        /// Some contetn for detailed presenting
        /// </summary>
        public UserControl Content { get; }
    }
}
