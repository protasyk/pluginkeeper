﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Common
{
    public class CommandBase : ICommand
    {

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        public CommandBase(Action<object> execute)
            : this(execute, null)
        {
        }

        public CommandBase(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
