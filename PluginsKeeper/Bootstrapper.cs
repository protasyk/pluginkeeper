﻿using Prism.Mef;
using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;

namespace PluginsKeeper
{
    public class Bootstrapper : MefBootstrapper
    {
        protected override void ConfigureAggregateCatalog()
        {
            var modules = Directory.GetFiles(Environment.CurrentDirectory, "*.dll");
            foreach (var assembly in modules)
            {
                AggregateCatalog.Catalogs.Add(new AssemblyCatalog(assembly));
            }
            AggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetEntryAssembly()));
        }

        protected override void ConfigureServiceLocator()
        {
            base.ConfigureServiceLocator();
        }

        protected override void InitializeShell()
        {
            base.InitializeShell();
        }
    }
}
