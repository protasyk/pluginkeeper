﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PluginsKeeper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Bootstrapper _bootst;
        protected override void OnStartup(StartupEventArgs e)
        {
            _bootst = new Bootstrapper();
            
            _bootst.Run();
        }        
    }
}
