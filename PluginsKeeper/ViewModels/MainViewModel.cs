﻿using Common;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;

namespace PluginsKeeper.ViewModels
{
    [Export]
    public class MainViewModel : ViewModelBase
    {
        private IPluginViewModel _selected;
        public MainViewModel()
        {
            Plugins = new ObservableCollection<IPluginViewModel>();
            var t = ServiceLocator.Current.GetAllInstances<IPluginViewModel>();
            foreach (var item in t)
            {
                Plugins.Add(item);
            }
        }

        /// <summary>
        /// Plugins collection
        /// </summary>
        public ObservableCollection<IPluginViewModel> Plugins { set; get; }

        public IPluginViewModel SelectedPlugin
        {
            get { return _selected; }
            set
            {
                _selected = value;
                RaisePropertyChanged("SelectedPlugin");
            }
        }
    }
}
